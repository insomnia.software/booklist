﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;
using NodaTime;

namespace BookList
{
	public class Book
	{
		//from google
		public string Title { get; set; }
		public string Subtitle { get; set; }
		public List<string> Authors { get; set; }
		public string Description { get; set; }
		//dimensions?
		public List<string> ImageLinks { get; } = new List<string>();
		public string Isbn10 { get; set; }
		public string Isbn13 { get; set; }
		public int PageCount { get; set; }
		public string PrintType { get; set; }
		public string PublicationDate { get; set; }
		
		//defined by me
		public DataSource DataSource { get; set; }
		[JsonIgnore]
		public LocalDate? DateStarted
		{
			get => Helpers.ParseLocalDateIso(DateStartedString);
			set => DateStartedString = value?.ToString("yyMMdd", CultureInfo.CurrentCulture);
		}
		[JsonIgnore]
		public LocalDate? DateFinished
		{
			get => Helpers.ParseLocalDateIso(DateFinishedString);
			set => DateFinishedString = value?.ToString("yyMMdd", CultureInfo.CurrentCulture);
		}

		public string DateStartedString { get; set; }
		public string DateFinishedString { get; set; }
		public bool Stopped { get; set; }

		[JsonIgnore]
		public CurrentState CurrentState
		{
			get
			{
				if (DateStarted == null)
					return CurrentState.NotStarted;
				if(DateFinished == null)
					return Stopped ? CurrentState.Stopped : CurrentState.NotFinished;
				return CurrentState.Finished;
			}
		}

		public string ThoughtsBefore { get; set; }
		public string ThoughtsAfter { get; set; }
		public string Catagory { get; set; }
		public Format Available { get; set; }
		public Format Owned { get; set; }
		public RatingInformation Ratings { get; set; }
		public SalesInformation SalesInfo { get; set; }
		
		public override string ToString()
		{
			return $"{Title} {Subtitle} by {Authors.First()} Cat: {Catagory} {DateStarted}{(DateFinished != null && DateStarted != null ? "-" : "")}{DateFinished}";
		}
	}
}