﻿namespace BookList
{
	public class RatingInformation
	{
		public float Rating { get; set; }
		public int NumberOfRatings { get; set; }
	}
}