﻿using System.Collections.Generic;

namespace BookList
{
	public interface IStorage
	{
		bool LoadBooks();
		bool Add(Book book);
		bool Save();
		bool Delete(Book book);
		List<Book> GetBooks();
	}
}