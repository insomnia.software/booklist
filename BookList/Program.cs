﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace BookList
{
	class Program
	{
		private static IStorage storage;
		private static IVisualisation visualisation;
		private static IRatingRepository ratingRepository;
		private static IFormatRepository formatRepository;
		private static IStatistics statistics;

		static void Main()
		{
			Log.Logging = new ConsoleLog();
			IApiStorage apiStorage;
			if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
				apiStorage = new ApiStorage("/Users/insomnia/S/A/Home/apiKeys.keydb");
			else
				apiStorage = new ApiStorage("C:/home/S/A/Home/apiKeys.keydb");

			IBookRepository bookRepository = new Google(apiStorage.GetApiKey("Google"));
			ratingRepository = new GoodreadsRatingRepository(apiStorage.GetApiKey("Goodreads"));
			formatRepository = new SalesInformation.FormatRepository();
			storage = new JsonStorage();
			visualisation = new HtmlVisualisation(storage);

			storage.LoadBooks();
			visualisation.ProduceVisualisation();
			statistics = new Statistics(storage.GetBooks());
			statistics.PrintBooksRead();
			statistics.PrintBooksReadByCategory();

			while (true)
			{
				string input = ConsoleHelper.GetUserInput("1. Add book\n2. Search books\n3. Edit book\n4. Refresh Html\n5. Refresh ratings\n6. Update books");
				switch (input)
				{
					case "1":
						AddBook(bookRepository);
						break;
					case "2":
						PrintBooksArguments();
						break;
					case "3":
						EditBook();
						break;
					case "4":
						visualisation.ProduceVisualisation();
						break;
					case "5":
						RefreshRatings();
						break;
					case "6":
						UpdateBooks(bookRepository);
						break;
				}
			}
		}

		private static void UpdateBooks(IBookRepository bookRepository)
		{
			foreach (Book book in storage.GetBooks())
			{
				bookRepository.UpdateBook(book);
			}

			storage.Save();
		}

		private static void PrintBooksArguments()
		{
			string s = "State: ";
			foreach (CurrentState state in Enum.GetValues(typeof(CurrentState)))
				s += $"{state.ToString()}: {(int) state}, ";
			Log.Logging.Log(s);
			s = "Format: ";
			foreach (Format format in Enum.GetValues(typeof(Format)))
				s += $"{format.ToString()}: {(int) format}, ";
			Log.Logging.Log(s);
			s = "Category: ";
			foreach (Catagory cat in Enum.GetValues(typeof(Catagory)))
				s += $"{cat.ToString()}: {(int) cat}, ";
			Log.Logging.Log(s);

			CurrentState? state1 = null;
			Format? format1 = null;
			Catagory? catagory1 = null;
			string title = null;
			
			string searchString = ConsoleHelper.GetUserInput("Enter search string [-s (int)state] [-f (int)format] [-c (int)category] [-n name]:");
			string[] args = searchString.Split(' ');
			for (var i = 0; i < args.Length; i++)
			{
				switch (args[i])
				{
					case "-s":
						state1 = (CurrentState)int.Parse(args[i + 1]);
						i++;
						break;
					case "-f":
						format1 = (Format)int.Parse(args[i + 1]);
						i++;
						break;
					case "-c":
						catagory1 = (Catagory)int.Parse(args[i + 1]);
						i++;
						break;
					case "-n":
						title = args[i + 1];
						break;
				}
			}
			
			PrintBooksWithFilters(state1, format1, catagory1, title);
		}

		private static void PrintBooksWithFilters(CurrentState? state = null, Format? availableOn = null, Catagory? catagory = null, string search = null)
		{
			List<Book> restrictedList = storage.GetBooks();

			if (state != null)
				restrictedList = restrictedList.Where(x => x.CurrentState == state).ToList();

			if(availableOn != null)
				restrictedList = restrictedList.Where(x => (int)(x.Available & availableOn.Value) == (int)availableOn.Value).ToList();
			
			if(catagory != null)
				restrictedList = restrictedList.Where(x => x.Catagory == catagory.ToString()).ToList();
			
			if(search != null)
				restrictedList = restrictedList.Where(x => x.Title.ToUpper().Contains(search.ToUpper())).ToList();
			
			foreach (Book book in restrictedList.OrderBy(x => x.Title))
				Log.Logging.Log(book.ToString());
		}

		private static void EditBook()
		{
			List<Book> alphabeticList = storage.GetBooks().OrderBy(x => x.Title).ToList();
			for (var i = 0; i < alphabeticList.Count; i++)
				Log.Logging.Log($"{i}: {alphabeticList[i]}");

			int input = ConsoleHelper.GetUserIntegerInput("Edit which book?");

			Book bookToEdit = alphabeticList[input];

			Log.Logging.Log($"Editing: {alphabeticList[input].Title}");
			Log.Logging.Log("1. Date started");
			Log.Logging.Log("2. Date finished");
			Log.Logging.Log("3. Owned catagories");
			Log.Logging.Log("4. Thoughts after");
			Log.Logging.Log("------------");
			Log.Logging.Log("5. Thoughts before");
			Log.Logging.Log("6. Available catagories");
			Log.Logging.Log("7. Catagory");
			Log.Logging.Log("8. Delete");

			input = ConsoleHelper.GetUserIntegerInput("Edit what attribute?");

			switch (input)
			{
				case 1://started
					bookToEdit.DateStarted = ConsoleHelper.GetUserDateInput("Date started:");
					break;
				case 2://finished
					bookToEdit.DateFinished = ConsoleHelper.GetUserDateInput("Date finished:");
					break;
				case 3://owned cat
					foreach (Format format in Enum.GetValues(typeof(Format)))
						Log.Logging.Log($"{format.ToString()}: {(int) format}");
					if (ConsoleHelper.GetUserBoolInput("Add format? Y/N (otherwise overwrite)").Value)
						bookToEdit.Owned |= (Format) ConsoleHelper.GetUserIntegerInput("Add what format?");
					else
					{
						string[] formats = ConsoleHelper.GetUserInput("Enter space separated list of numbers").Split(' ');
						foreach (string format in formats)
							bookToEdit.Owned |= (Format) int.Parse(format);
					}
					break;
				case 4://after thoughts
					bookToEdit.ThoughtsAfter = ConsoleHelper.GetUserInput("Thoughts after reading the book:");
					break;
				case 5://before thoughts
					bookToEdit.ThoughtsBefore = ConsoleHelper.GetUserInput("Thoughts before reading the book:");
					break;
				case 6://available cat
					foreach (Format format in Enum.GetValues(typeof(Format)))
						Log.Logging.Log($"{format.ToString()}: {(int) format}");
					string[] formats2 = ConsoleHelper.GetUserInput("Enter space separated list of numbers").Split(' ');
					foreach (string format in formats2)
						bookToEdit.Owned |= (Format) int.Parse(format);
					break;
				case 7://cat
					foreach (Catagory catagory in Enum.GetValues(typeof(Catagory)))
						Log.Logging.Log($"{catagory.ToString()}: {(int) catagory}");

					bookToEdit.Catagory = ((Catagory)ConsoleHelper.GetUserIntegerInput("Enter catagory:")).ToString();
					Log.Logging.Log($"Catagory: {bookToEdit.Catagory}");
					break;
				case 8://delete
					storage.Delete(bookToEdit);
					break;
			}

			storage.Save();
			visualisation.ProduceVisualisation();
		}

		private static void AddBook(IBookRepository br)
		{
			bool retry = false;

			Book book;
			string isbn;
			do
			{
				isbn = ConsoleHelper.GetUserInput("Enter ISBN:").Replace("-", "");

				book = br.GetBookByIsbn(isbn);

				if (book == null)
				{
					bool? answer = ConsoleHelper.GetUserBoolInput($"Book with isbn {isbn} not found. Try again? (Y/N): ");
					if (answer != null && answer.Value)
						retry = true;
				}
				else // need this to reset the bol if the first was a fail and the second succeeds
				{
					retry = false;
				}
			} while (retry);

			if (book == null)
			{
				Log.Logging.Log($"Book with isbn {isbn} not found. Enter information manually");
				book = new Book();
				book.Title = ConsoleHelper.GetUserInput("Enter book title:");
				book.Subtitle = ConsoleHelper.GetUserInput("Enter book subtitle:");
				book.Authors = ConsoleHelper.GetUserInput("Enter authors (comma separated, no spaces):").Split(',').ToList();
				book.Description = ConsoleHelper.GetUserInput("Enter book description:");
				book.PublicationDate = ConsoleHelper.GetUserInput("Enter book publication date:");
				book.DataSource = DataSource.Manual;
			}
			else
			{
				Log.Logging.Log($"Found: {book.Title}\n");	
			}

			foreach (Format format in Enum.GetValues(typeof(Format)))
				Log.Logging.Log($"{format.ToString()}: {(int) format}");

			string[] formats = ConsoleHelper.GetUserInput("What catagories is this book available in? Enter space separated list of numbers").Split(' ');
			foreach (string format in formats)
				book.Available |= (Format)int.Parse(format);

			Log.Logging.Log(book.Available.ToString());

//			book.SalesInfo = formatRepository.GetAvailabilityByIsbn(isbn);
//
//			foreach (SalesInformation.FormatPrice salesInfoPrice in book.SalesInfo.prices)
//				Log.Logging.Log($"{salesInfoPrice.Format}: £{salesInfoPrice.Price}");

			formats = ConsoleHelper.GetUserInput("What catagories do you have this book in? Enter space separated list of numbers").Split(' ');
			foreach (string format in formats)
				if(format != "")
					book.Owned |= (Format) int.Parse(format);

			Log.Logging.Log(book.Owned.ToString());

			book.DateStarted = ConsoleHelper.GetUserDateInput("Enter date started:");

			book.DateFinished = ConsoleHelper.GetUserDateInput("Enter date finished:");

			foreach (Catagory catagory in Enum.GetValues(typeof(Catagory)))
				Log.Logging.Log($"{catagory.ToString()}: {(int) catagory}");

			book.Catagory = ((Catagory)ConsoleHelper.GetUserIntegerInput("Enter catagory:")).ToString();
			Log.Logging.Log($"Catagory: {book.Catagory}");

			book.ThoughtsBefore = ConsoleHelper.GetUserInput($"Why is this book of interest?");
			
			Log.Logging.Log("Getting rating information...");
			book.Ratings = ratingRepository.GetRatingsByIsbn(isbn);
			Log.Logging.Log(book.Ratings == null
				? "Failed to get rating information"
				: $"Rating: {book.Ratings.Rating} based on {book.Ratings.NumberOfRatings} ratings.");

			storage.Add(book);
			visualisation.ProduceVisualisation();
		}

		private static void RefreshRatings()
		{
			foreach (Book book in storage.GetBooks())
			{
				Log.Logging.Log($"Getting rating information for {book.Title}");
				book.Ratings = ratingRepository.GetRatingsByIsbn(book.Isbn13);
				if (book.Ratings == null)
					Log.Logging.Log("Failed to get rating information");
				else
					Log.Logging.Log($"Rating: {book.Ratings.Rating} based on {book.Ratings.NumberOfRatings} ratings.");
			}

			storage.Save();
		}
	}
}