﻿using System;

namespace BookList
{
	public interface ILog
	{
		void Log(string message, Severity severity = Severity.Info, Exception exception = null);
	}

	public sealed class ConsoleLog : ILog
	{
		public void Log(string message, Severity severity = Severity.Info, Exception exception = null)
		{
			Console.WriteLine($"{severity}: {message}");
			if(exception != null)
				Console.WriteLine(exception);
		}
	}

	public enum Severity
	{
		Debug,
		Info,
		Error
	}

	public static class Log
	{
		public static ILog Logging { get; set; }
	}
}