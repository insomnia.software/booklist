﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace BookList
{
	public class HtmlVisualisation : IVisualisation
	{
		private readonly IStorage _storage;

		public HtmlVisualisation(IStorage storage)
		{
			_storage = storage;
		}
		
		public void ProduceVisualisation()
		{
			List<Book> books = _storage.GetBooks().OrderBy(x => x.DateStarted == null).ThenByDescending((x => x.DateStarted)).ToList();
			string s = File.ReadAllText("input.html");
			string table = "";
			for (var index = 0; index < books.Count; index++)
			{
				Book book = books[index];
				//<tr><thscope="row">1</th><td>Mark</td><td>Otto</td><td>@mdo</td></tr>

				string tableRowClass = book.CurrentState == CurrentState.Finished || book.CurrentState == CurrentState.Stopped ? " class=\"table-success\"" : "";

				switch (book.CurrentState)
				{
					case CurrentState.Stopped:
						tableRowClass = " class=\"table-active\"";
						break;
					case CurrentState.Finished:
						tableRowClass = " class=\"table-success\"";
						break;
					case CurrentState.NotFinished:
						tableRowClass = " class=\"table-info\"";
						break;
				}
				
				string tableDataIndex = (index + 1).ToString();
				string tableOwnedFormats = FormatHelper.ToShortString(book.Owned);
				string tableAvailableFormats = FormatHelper.ToShortString(book.Available);
				string tableDateStarted = book.DateStarted?.ToString("dd/MM/yy", CultureInfo.CurrentCulture);
				string tableDateFinished = book.DateFinished?.ToString("dd/MM/yy", CultureInfo.CurrentCulture);
				string ratingInfo = book.Ratings == null ? "" : $"{book.Ratings?.Rating.ToString("0.00")}({IntFormatter(book.Ratings.NumberOfRatings)})";
				
				table += $"<tr{tableRowClass}><th scope=\"row\">{tableDataIndex}</th>" +
				         $"<td>{book.Title} ({book.PublicationDate?.Split('-')[0]})<br/>{book.Subtitle}</td><td>{book.Authors.First()}</td>" +
				         $"<td>{ratingInfo}</td>" +
				         $"<td>{book.Catagory}</td>" +
				         $"<td>{tableAvailableFormats}</td>" +
				         $"<td>{tableOwnedFormats}</td>" +
				         $"<td>{tableDateStarted}</td>" +
				         $"<td>{tableDateFinished}</td></tr>\n";
			}

			s = s.Replace("REPLACE_THIS_TEXT", table);
			
			//<select><optionvalue="volvo">Volvo</option><optionvalue="saab">Saab</option><optionvalue="mercedes">Mercedes</option><optionvalue="audi">Audi</option></select
			string categoryComboBox = "<select id=\"categoryInput\" onchange=\"filter()\"><option value=\"\" selected>Select catagory</option>";
			foreach (Catagory cat in Enum.GetValues(typeof(Catagory)))
				categoryComboBox += $"<option value=\"{cat}\">{cat}</option>";
			categoryComboBox += "</select>";
			
			string formatAvailableComboBox	= "<select id=\"formatInput\" onchange=\"filter()\"><option value=\"\" selected>Select format</option>";
			foreach (Format format in Enum.GetValues(typeof(Format)).OfType<Format>().Skip(1))
				formatAvailableComboBox += $"<option value=\"{format}\">{format}</option>";
			formatAvailableComboBox += "</select>";

			categoryComboBox += formatAvailableComboBox;

			s = s.Replace("COMBO_BOXES_HERE", categoryComboBox);
			
			using (TextWriter tw = new StreamWriter("output.html"))
			{
				tw.Write(s);
			}
			
			Log.Logging.Log("Produced new Html");
		}

		public static string IntFormatter(int i)
		{
			if (i < 1000)
				return i.ToString();
			if (i < 1_000_000)
			{
				decimal rounded = Math.Round((decimal) i / 1000, 0, MidpointRounding.AwayFromZero) * 1000;
				decimal shortened = rounded / 1000;

				if (shortened == 1000)
					return "1M";
				return $"{shortened}K";
			}
			if (i < 1_000_000_000)
			{
				decimal rounded = Math.Round((decimal) i / 1_000_000, 0, MidpointRounding.AwayFromZero) * 1_000_000;
				decimal shortened = rounded / 1_000_000;

				if (shortened == 1_000_000)
					return "1B";
				return $"{shortened}M";
			}
			return "lots";
		}
	}
}