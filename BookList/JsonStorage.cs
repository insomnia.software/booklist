﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace BookList
{
	public class JsonStorage : IStorage
	{
		private const string DB = "database.bookdb";
		private List<Book> books;
		
		public bool LoadBooks()
		{
			if (File.Exists(DB))
			{
				books = JsonConvert.DeserializeObject<List<Book>>(File.ReadAllText(DB));
				Log.Logging.Log($"Found book database. Loaded in {books.Count} books.");
				
				return true;
			}

			Log.Logging.Log($"Failed to find file {DB}.");

			return false;
		}

		public bool Add(Book book)
		{
			//validate?
			books.Add(book);

			Save();

			return true;
		}

		public bool Save()
		{
			string jsonOutput = JsonConvert.SerializeObject(books, Formatting.Indented);

			using (TextWriter tw = new StreamWriter(DB))
			{
				tw.Write(jsonOutput);
			}
			Log.Logging.Log("Saved database successfully");
			return true;
		}

		public bool Delete(Book book)
		{
			return books.Remove(book) && Save();
		}

		public List<Book> GetBooks()
		{
			return books;
		}
	}
}