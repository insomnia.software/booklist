﻿namespace BookList
{
	public interface IVisualisation
	{
		void ProduceVisualisation();
	}
}