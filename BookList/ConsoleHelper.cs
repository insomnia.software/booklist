﻿using System;
using System.Runtime.InteropServices;
using NodaTime;

namespace BookList
{
	public static class ConsoleHelper
	{
		public static string GetUserInput(string question)
		{
			Log.Logging.Log(question);
			return Console.ReadLine();
		}
		
		public static int GetUserIntegerInput(string question) => int.Parse(GetUserInput(question));

		public static LocalDate? GetUserDateInput(string question) => Helpers.ParseLocalDateIso(GetUserInput(question));

		public static bool? GetUserBoolInput(string question)
		{
			switch (GetUserInput(question).ToUpper())
			{
				case "Y":
				case "YES":
					return true;
				case "N":
				case "NO":
					return false;
				default:
					return null;
			}
		}
	}
}