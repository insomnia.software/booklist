﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Books.v1;
using Google.Apis.Books.v1.Data;
using Google.Apis.Services;

namespace BookList
{
	public class Google : IBookRepository
	{
		private BooksService BookService { get; }
		
		public Google(string apiKey)
		{
			BookService = new BooksService
			(
				new BaseClientService.Initializer
				{
					ApplicationName = "BookCatalog",
					ApiKey = apiKey
				}
			);
		}

		private async Task<Volume> SearchISBN(string isbn)
		{
			Log.Logging.Log("Executing a book search request…");
			var v = BookService.Volumes.List(isbn);
			//v.LangRestrict = "en";
			v.MaxResults = 40;
			Volumes result = await v.ExecuteAsync();

			for (var index = 0; index < result.Items.Count; index++)
			{
				Volume volume = result.Items[index];

				string isbns = "None";
				if (volume.VolumeInfo.IndustryIdentifiers != null)
					isbns = string.Join(", ", volume.VolumeInfo.IndustryIdentifiers.Select(x => x.Identifier));
				
				Console.WriteLine($"{index}: {volume.VolumeInfo.Title}: {isbns}");
			}

			Volume item = result?.Items?.FirstOrDefault(x => x.VolumeInfo.IndustryIdentifiers?.FirstOrDefault(y => y.Identifier == isbn) != null);
			
			return item;
		}

		public Book GetBookByIsbn(string isbn)
		{
			Volume v = SearchISBN(isbn).Result;

			if (v == null)
				return null;
			
			Book b = new Book();
			b.Title = v.VolumeInfo.Title;
			b.Subtitle = v.VolumeInfo.Subtitle;
			b.Authors = v.VolumeInfo.Authors.ToList();
			b.Description = v.VolumeInfo.Description;
			b.PublicationDate = v.VolumeInfo.PublishedDate;
			foreach (Volume.VolumeInfoData.IndustryIdentifiersData identifier in v.VolumeInfo.IndustryIdentifiers)
			{
				if (identifier.Type == "ISBN_10")
					b.Isbn10 = identifier.Identifier;
				if (identifier.Type == "ISBN_13")
					b.Isbn13 = identifier.Identifier;
			}
			if(v.VolumeInfo.ImageLinks?.SmallThumbnail != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.SmallThumbnail);
			if(v.VolumeInfo.ImageLinks?.Thumbnail != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.Thumbnail);
			if(v.VolumeInfo.ImageLinks?.Small != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.Small);
			if(v.VolumeInfo.ImageLinks?.Medium != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.Medium);
			if(v.VolumeInfo.ImageLinks?.Large != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.Large);
			if(v.VolumeInfo.ImageLinks?.ExtraLarge != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.ExtraLarge);
			b.PageCount = v.VolumeInfo.PageCount ?? 0;
			b.PrintType = v.VolumeInfo.PrintType;
			b.DataSource = DataSource.Google;
			return b;
		}

		public void UpdateBook(Book b)
		{
			Log.Logging.Log($"Updating book {b.Title}");
			Volume v = SearchISBN(b.Isbn13).Result;

			if (v == null)
			{
				Log.Logging.Log($"Failed to find book with isbn: {b.Isbn13}");
				return;
			}

			b.Title = v.VolumeInfo.Title;
			b.Subtitle = v.VolumeInfo.Subtitle;
			b.Authors = v.VolumeInfo.Authors.ToList();
			b.Description = v.VolumeInfo.Description;
			b.PublicationDate = v.VolumeInfo.PublishedDate;
			foreach (Volume.VolumeInfoData.IndustryIdentifiersData identifier in v.VolumeInfo.IndustryIdentifiers)
			{
				if (identifier.Type == "ISBN_10")
					b.Isbn10 = identifier.Identifier;
				if (identifier.Type == "ISBN_13")
					b.Isbn13 = identifier.Identifier;
			}
			if (v.VolumeInfo.ImageLinks.SmallThumbnail != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.SmallThumbnail);
			if (v.VolumeInfo.ImageLinks.Thumbnail != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.Thumbnail);
			if (v.VolumeInfo.ImageLinks.Small != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.Small);
			if (v.VolumeInfo.ImageLinks.Medium != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.Medium);
			if (v.VolumeInfo.ImageLinks.Large != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.Large);
			if (v.VolumeInfo.ImageLinks.ExtraLarge != null)
				b.ImageLinks.Add(v.VolumeInfo.ImageLinks.ExtraLarge);
			b.PageCount = v.VolumeInfo.PageCount ?? 0;
			b.PrintType = v.VolumeInfo.PrintType;
		}
	}
}