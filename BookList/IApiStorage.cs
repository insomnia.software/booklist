﻿namespace BookList
{
	public interface IApiStorage
	{
		string GetApiKey(string name);
	}
}