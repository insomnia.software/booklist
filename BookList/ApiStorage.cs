﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Resources;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace BookList
{
	public class ApiStorage : IApiStorage
	{
		private const string DEFAULT_API_FILE = "default.keydb";
		private string StorageLocation { get; }
		private Dictionary<string, string> ApiKeys { get; set; }

		public ApiStorage(string storageLocation = null)
		{
			if (!string.IsNullOrWhiteSpace(storageLocation))
			{
				StorageLocation = storageLocation;
				Log.Logging.Log($"Loading in key database file {storageLocation}");
			}
			else
			{
				StorageLocation = DEFAULT_API_FILE;
				if (!File.Exists(StorageLocation))
				{
					Log.Logging.Log($"Can't find default api key file and no filepath loaded in, creating a default one");
					CreateDefualtApiKeyDatabase();
				}
			}
			LoadKeyFile(StorageLocation);
		}
		
		public string GetApiKey(string name)
		{
			if (ApiKeys == null)
				return null;
			return ApiKeys.ContainsKey(name) ? ApiKeys[name] : null;
		}

		private void LoadKeyFile(string filePath)
		{
			if (File.Exists(filePath))
			{
				ApiKeys = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(filePath));
				Log.Logging.Log($"Found API key database. Loaded in keys for {string.Join(", ", ApiKeys.Select(x => x.Key))}.");
			}
			else
			{
				Log.Logging.Log($"Failed to find file {filePath}");
			}
		}

		private void CreateDefualtApiKeyDatabase()
		{
			Dictionary<string, string> defaultApiKeys = new Dictionary<string, string>{{"ExampleServiceName1", "ExampleKey1"}, {"ExampleServiceName2", "ExampleKey2"}};
			string jsonOutput = JsonConvert.SerializeObject(defaultApiKeys, Formatting.Indented);
			using (TextWriter tw = new StreamWriter(DEFAULT_API_FILE))
			{
				tw.Write(jsonOutput);
			}
		}
	}
}