﻿using System;
using System.Xml.Schema;

namespace BookList
{
	public interface IRatingRepository
	{
		RatingInformation GetRatingsByIsbn(string isbn);
	}
}