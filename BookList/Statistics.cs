﻿using System.Collections.Generic;
using System.Linq;

namespace BookList
{
	public interface IStatistics
	{
		void PrintBooksRead();

		void PrintBooksReadByCategory();
	}
	
	public class Statistics : IStatistics
	{
		private readonly List<Book> books;

		public Statistics(List<Book> books)
		{
			this.books = books;
		}

		public void PrintBooksRead()
		{
			Log.Logging.Log($"Found {books.Count} books, {books.Count(x => x.DateFinished != null)} finished, {books.Count(x => x.Stopped)} stopped and {books.Count(x => x.DateStarted == null)} not started.");
		}

		public void PrintBooksReadByCategory()
		{
			IEnumerable<Book> booksRead = books.Where(x => x.DateFinished != null || x.Stopped);
			
			Dictionary<string, int> countOfBooksReadByCategory = new Dictionary<string, int>();

			foreach (Book book in booksRead)
			{
				if (!countOfBooksReadByCategory.ContainsKey(book.Catagory))
					countOfBooksReadByCategory.Add(book.Catagory, 1);
				else
					countOfBooksReadByCategory[book.Catagory]++;
			}
			
			IOrderedEnumerable<KeyValuePair<string, int>> v = countOfBooksReadByCategory.OrderByDescending(x => x.Value);

			foreach (KeyValuePair<string,int> kvp in v)
			{
				Log.Logging.Log($"{kvp.Key}: {kvp.Value}");
			}
		}
	}
}