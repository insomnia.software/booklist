﻿namespace BookList
{
	public interface IBookRepository
	{
		Book GetBookByIsbn(string isbn);
		void UpdateBook(Book book);
	}
}