﻿using System;
using NodaTime;
using NodaTime.Text;

namespace BookList
{
	public static class Helpers
	{
		public static LocalDate? ParseLocalDateIso(string input)
		{
			if (string.IsNullOrWhiteSpace(input))
				return null;
			LocalDatePattern localDatePattern = LocalDatePattern.CreateWithCurrentCulture("yyMMdd");
			ParseResult<LocalDate> parseResult = localDatePattern.Parse(input);

			if (parseResult.Success)
				return parseResult.Value;
			return null;
		}
	}
}