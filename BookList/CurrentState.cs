﻿namespace BookList
{
	public enum CurrentState
	{
		NotStarted,
		NotFinished,
		Finished,
		Stopped
	}
}