﻿using System;
using System.Collections.Generic;

namespace BookList
{
	[Flags]
	public enum Format
	{
		None = 0,
		Physical = 1,
		Ebook = 2,
		Audible = 4,
		Audiobook = 8,
		CustomAudiobook = 16
	}

	public static class FormatHelper
	{
		private static Dictionary<Format, string> _shortNames = new Dictionary<Format, string>
		{
			{Format.None, ""},
			{Format.Physical, "P"},
			{Format.Ebook, "E"},
			{Format.Audible, "A"},
			{Format.Audiobook, "CD"},
			{Format.CustomAudiobook, "C"}
		};
		
		public static string ToShortString(Format f)
		{
			string toReturn = "";
			for (int i = 0; i < 5; i++)
			{
				int lol = 1 << i;
				if (f.HasFlag((Format)lol))
				{
					if (toReturn != "")
						toReturn += $", {_shortNames[(Format)lol]}";
					else
						toReturn += _shortNames[(Format)lol];
				}
			}

			return toReturn;
		}
	}
}