﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using NodaTime;

namespace BookList
{
	public class GoodreadsRatingRepository : IRatingRepository
	{
		public GoodreadsRatingRepository(string apiKey)
		{
			Key = apiKey;
		}
		
		private string Key { get; }
		private Instant lastCallTime = Instant.MinValue;
		
		public RatingInformation GetRatingsByIsbn(string isbn)
		{
			Instant now = SystemClock.Instance.GetCurrentInstant();
			Duration timeSinceLastCall = now - lastCallTime;
			if (timeSinceLastCall < Duration.FromSeconds(1))
			{
				Thread.Sleep(1000 - (int)timeSinceLastCall.TotalMilliseconds);
			}
			
			lastCallTime = SystemClock.Instance.GetCurrentInstant();
			string url = $"https://www.goodreads.com/book/isbn/{isbn}?key={Key}";
			
			WebRequest request = WebRequest.Create(url);
			WebResponse response;
			try
			{
				response = request.GetResponse();
			}
			catch (WebException e)
			{
				Log.Logging.Log("Couldn't find book on Goodreads");
				return null;
			}
			Stream data = response.GetResponseStream();
			
			var xdoc = XDocument.Load(data);
			var goodreadsResponse = (XElement)xdoc.FirstNode;

			List<XElement> elements = GetDirectChildElements(goodreadsResponse.FirstNode);//request and book
			XElement book = elements.FirstOrDefault(x => x.Name == "book");

			var bookChildren = GetDirectChildElements(book.FirstNode);
			string averageRatingString = bookChildren.FirstOrDefault(x => x.Name == "average_rating")?.Value;
			XElement work = bookChildren.FirstOrDefault(x => x.Name == "work");

			List<XElement> workChildren = GetDirectChildElements(work.FirstNode);
			string numberOfRatingsString = workChildren.FirstOrDefault(x => x.Name == "ratings_count")?.Value;


			int numberOfRatings = 0;
			float averageRating = 0f;
			bool success = int.TryParse(numberOfRatingsString, out numberOfRatings);
			success = success && float.TryParse(averageRatingString, out averageRating);
			
			if(!success)
				return null;

			return new RatingInformation {NumberOfRatings = numberOfRatings, Rating = averageRating};
		}

		private List<XElement> GetDirectChildElements(XNode node)
		{
			List<XElement> elements = new List<XElement>();

			while (node != null)
			{
				if (node.NodeType == XmlNodeType.Element)
					elements.Add((XElement) node);
				node = node.NextNode;
			}

			return elements;
		}
	}
}