﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using NodaTime;

namespace BookList
{
	public interface IFormatRepository
	{
		SalesInformation GetAvailabilityByIsbn(string isbn);
	}

	public enum SaleFormat
	{
		Unknown,
		Paperback,
		Hardback,
		Ebook,
		Audible,
		CD
	}

	public class SalesInformation
	{
		public Instant LastPriceCheck { get; set; }
		public List<FormatPrice> prices { get; set; }

		public class FormatPrice
		{
			public SaleFormat Format { get; set; }
			public float Price { get; set; }
		}

		public static class SalesHelper
		{
			public static string ToShortString(List<FormatPrice> f)
			{
				List<string> shortNames = new List<string>();

				foreach (FormatPrice salesInformation in f)
				{
					switch (salesInformation.Format)
					{
						case SaleFormat.Unknown:
							break;
						case SaleFormat.Paperback:
						case SaleFormat.Hardback:
							shortNames.Add("P");
							break;
						case SaleFormat.Ebook:
							shortNames.Add("E");
							break;
						case SaleFormat.Audible:
							shortNames.Add("A");
							break;
						case SaleFormat.CD:
							shortNames.Add("CD");
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}

				return string.Join(", ", shortNames);
			}
		}

		public class FormatRepository : IFormatRepository
		{
			public SalesInformation GetAvailabilityByIsbn(string isbn)
			{
				SalesInformation information = new SalesInformation();
				List<FormatPrice> prices = new List<FormatPrice>();

				string url = $"https://www.amazon.co.uk/dp/{isbn}";
				Log.Logging.Log($"Loading {url}");
				WebRequest request = WebRequest.Create(url);
				WebResponse response = request.GetResponse();
				Stream data = response.GetResponseStream();

				string html;
				using (StreamReader sr = new StreamReader(data))
					html = sr.ReadToEnd();

				string kindle = GetPriceOfFormat("Kindle Edition", html);
				string hardback = GetPriceOfFormat("Hardcover", html);
				string paperback = GetPriceOfFormat("Paperback", html);
				string audio = GetPriceOfFormat("Audio CD", html);
				bool audible = Regex.IsMatch(html, "with your Audible trial");

				if (kindle != null)
					prices.Add(new FormatPrice {Format = SaleFormat.Ebook, Price = float.Parse(kindle)});
				if (hardback != null)
					prices.Add(new FormatPrice {Format = SaleFormat.Hardback, Price = float.Parse(hardback)});
				if (paperback != null)
					prices.Add(new FormatPrice {Format = SaleFormat.Paperback, Price = float.Parse(paperback)});
				if (audio != null)
					prices.Add(new FormatPrice {Format = SaleFormat.CD, Price = float.Parse(audio)});
				if (audible)
					prices.Add(new FormatPrice {Format = SaleFormat.Audible, Price = 0});

				information.prices = prices;
				information.LastPriceCheck = SystemClock.Instance.GetCurrentInstant();
				return information;
			}

			private string GetPriceOfFormat(string format, string html)
			{
				Match match = Regex.Match(html, $"<span>{format}</span>(?:.|\n)+?£([0-9]+\\.[0-9]+)");
				if (match.Groups.Count > 1)
				{
					Log.Logging.Log($"Format: {format} Price: £{match.Groups[1]}");
					return match.Groups[1].ToString();
				}
				return null;
			}
		}
	}
}